﻿using FFXIVClientStructs.FFXIV.Client.System.String;
using FFXIVClientStructs.FFXIV.Client.UI;
using FFXIVClientStructs.FFXIV.Client.UI.Shell;

namespace NextUIPlugin.Service;

public unsafe partial struct NUShellCommandModule {
	public static RaptureShellModule* Instance() => UIModule.Instance()->GetRaptureShellModule();

	public static void ExecuteCommand(string command) {
		var cmd = stackalloc Utf8String[1];
		cmd->Ctor();
		cmd->SetString(command);
		Instance()->ExecuteCommandInner(cmd, UIModule.Instance());
		cmd->Dtor();
	}
}