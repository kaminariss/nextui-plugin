﻿using System;
using System.Runtime.InteropServices;
using Dalamud.Game.ClientState.Objects.Enums;
using Dalamud.Game.ClientState.Objects.SubKinds;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Logging;
using FFXIVClientStructs.FFXIV.Client.UI;
using FFXIVClientStructs.FFXIV.Client.UI.Agent;
using Fleck;
using NextUIPlugin.Service;
using NextUIPlugin.Socket;

namespace NextUIPlugin.Data.Handlers {
	public static unsafe class ContextHandler {
		internal static class Signatures {
			internal const string SendTellCommandSig = "E8 ?? ?? ?? ?? EB 23 48 8B 93";
			internal const string ExecuteCommandSig = "E8 ?? ?? ?? ?? 8D 46 0A";
		}

		internal delegate void SendTellCommandDelegate(
			long raptureModulePointer, byte* characterName, ushort homeWorldId
		);

		internal delegate void ExecuteCommandDelegate(
			int id, int a1, int a2, int a3, int a4
		);

		internal static SendTellCommandDelegate? SendTellCommand { get; set; }
		internal static ExecuteCommandDelegate? ExecuteCommand { get; set; }
		internal static UIModule* uiModule;

		public static void RegisterCommands() {
			uiModule = (UIModule*)NextUIPlugin.gameGui.GetUIModule();
			var sendTellPtr = NextUIPlugin.sigScanner.ScanText(Signatures.SendTellCommandSig);
			if (sendTellPtr != IntPtr.Zero) {
				SendTellCommand = Marshal.GetDelegateForFunctionPointer<SendTellCommandDelegate>(sendTellPtr);
			}
			else {
				NextUIPlugin.NULog.Warning("Signature for Send Tell Not found");
			}

			var execCommandPtr = NextUIPlugin.sigScanner.ScanText(Signatures.ExecuteCommandSig);
			if (execCommandPtr != IntPtr.Zero) {
				ExecuteCommand = Marshal.GetDelegateForFunctionPointer<ExecuteCommandDelegate>(execCommandPtr);
			}
			else {
				NextUIPlugin.NULog.Warning("Signature for Execute Command Not found");
			}

			NextUISocket.RegisterCommand("examine", Examine);
			NextUISocket.RegisterCommand("leaveParty", LeaveParty);
			NextUISocket.RegisterCommand("disbandParty", DisbandParty);
			NextUISocket.RegisterCommand("sendTell", SendTell);
			NextUISocket.RegisterCommand("resetEnmity", ResetEnmity);

			NextUISocket.RegisterCommand("showEmoteWindow", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/emotelist");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("showSignsWindow", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/enemysign");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("inviteToParty", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/invite <target>");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("meldRequest", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/meldrequest");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("tradeRequest", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/trade");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("followTarget", (socket, ev) => {
				NUShellCommandModule.ExecuteCommand("/follow");
				NextUISocket.Respond(socket, ev, new { success = true });
			});

			NextUISocket.RegisterCommand("promotePartyMember", (socket, ev) => {
				PartyLeaderOperation(socket, ev, "leader");
			});

			NextUISocket.RegisterCommand("kickFromParty", (socket, ev) => {
				PartyLeaderOperation(socket, ev, "kick");
			});
		}

		public static void Examine(IWebSocketConnection socket, SocketEvent ev) {
			var objectId = ev.request?.id ?? 0;
			if (objectId == 0) {
				return;
			}

			var obj = NextUIPlugin.SearchObjectTable(objectId);
			if (obj != null && obj.ObjectKind == ObjectKind.Player) {
				AgentInspect.Instance()->ExamineCharacter(obj.EntityId);

				NextUISocket.Respond(socket, ev, new { success = true });
				return;
			}

			NextUISocket.Respond(socket, ev, new { success = false, message = "Invalid object" });
		}

		internal static void LeaveParty(IWebSocketConnection socket, SocketEvent ev) {
			if (!IsInParty()) {
				NextUISocket.Respond(socket, ev, new { success = false, message = "Not in party" });
				return;
			}

			NUShellCommandModule.ExecuteCommand("/leave");

			NextUISocket.Respond(socket, ev, new { success = true });
		}

		internal static void DisbandParty(IWebSocketConnection socket, SocketEvent ev) {
			if (!IsInParty()) {
				NextUISocket.Respond(socket, ev, new { success = false, message = "Not in party" });
				return;
			}

			if (IsPartyLeader()) {
				NextUISocket.Respond(socket, ev, new { success = false, message = "Not a party leader" });
				return;
			}

			NUShellCommandModule.ExecuteCommand("/partycmd breakup");

			NextUISocket.Respond(socket, ev, new { success = true });
		}

		internal static void PartyLeaderOperation(IWebSocketConnection socket, SocketEvent ev, string op) {
			var objectId = ev.request?.id ?? 0;
			if (objectId == 0) {
				return;
			}

			if (NextUIPlugin.partyList.Length == 0) {
				NextUISocket.Respond(socket, ev, new { success = false, message = "Not in party" });
				return;
			}

			if (IsPartyLeader()) {
				NextUISocket.Respond(socket, ev, new { success = false, message = "Not a party leader" });
				return;
			}

			var obj = NextUIPlugin.SearchObjectTable(objectId);
			if (obj != null && obj is IPlayerCharacter character) {
				var index = GetPlayerPartyIndex(character);
				if (index is > 0 and < 9) {
					NUShellCommandModule.ExecuteCommand($"/{op} <{index}>");
					NextUISocket.Respond(socket, ev, new { success = true });
					return;
				}
			}

			NextUISocket.Respond(socket, ev, new { success = false });
		}

		internal static void SendTell(IWebSocketConnection socket, SocketEvent ev) {
			var objectId = ev.request?.id ?? 0;
			if (objectId == 0) {
				return;
			}

			var obj = NextUIPlugin.SearchObjectTable(objectId);

			if (obj != null && SendTellCommand != null && obj is IPlayerCharacter player) {
				var raptureShellModulePointer = (IntPtr)uiModule->GetRaptureShellModule();
				var rap = raptureShellModulePointer.ToInt64();

				var gameObject = (FFXIVClientStructs.FFXIV.Client.Game.Object.GameObject*)player.Address;
				SendTellCommand(rap, gameObject->GetName(), (ushort)player.HomeWorld.RowId);

				NextUISocket.Respond(socket, ev, new { success = true });
				return;
			}

			NextUISocket.Respond(socket, ev, new { success = false, message = "Invalid object" });
		}

		internal static void ResetEnmity(IWebSocketConnection socket, SocketEvent ev) {
			var objectId = ev.request?.id ?? 0;
			if (objectId == 0) {
				return;
			}

			var obj = NextUIPlugin.SearchObjectTable(objectId);

			// 541 is target dummy
			if (obj != null && ExecuteCommand != null && obj is IBattleChara chara && chara.NameId == 541) {
				NextUIPlugin.NULog.Information($"Resetting enmity {chara.EntityId:X}");
				ExecuteCommand(0x140, (int)chara.EntityId, 0, 0, 0);

				NextUISocket.Respond(socket, ev, new { success = true });
				return;
			}

			NextUISocket.Respond(socket, ev, new { success = false, message = "Invalid object" });
		}


		internal static int? GetPlayerPartyIndex(IPlayerCharacter character) {
			var agentHud = uiModule->GetAgentModule()->GetAgentHUD();
			var list = agentHud->PartyMembers;

			for (var i = 0; i < (short)agentHud->PartyMemberCount; i++) {
				var partyMember = list[i];
				if (partyMember.EntityId != character.EntityId) {
					continue;
				}

				return i + 1;
			}

			return null;
		}

		internal static bool IsInParty() {
			return NextUIPlugin.partyList.Length > 0;
		}

		internal static bool IsPartyLeader() {
			var partyLeaderIndex = (int)NextUIPlugin.partyList.PartyLeaderIndex;
			var partyLeaderId = NextUIPlugin.partyList[partyLeaderIndex]?.ObjectId;
			return partyLeaderId != NextUIPlugin.clientState.LocalPlayer?.EntityId;
		}
	}
}