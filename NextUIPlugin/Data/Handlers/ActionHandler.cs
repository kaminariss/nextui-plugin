﻿using Fleck;
using Lumina.Excel;
using Lumina.Excel.Sheets;
using NextUIPlugin.Socket;

namespace NextUIPlugin.Data.Handlers {
	public static class ActionHandler {
		internal static ExcelSheet<Action>? actionSheet;

		public static void RegisterCommands() {
			actionSheet = NextUIPlugin.dataManager.GetExcelSheet<Action>();
			NextUISocket.RegisterCommand("getAction", GetAction);
		}

		internal static void GetAction(IWebSocketConnection socket, SocketEvent ev) {
			var objectId = ev.request?.id ?? 0;
			if (objectId == 0 || actionSheet == null) {
				return;
			}

			var action = actionSheet.GetRow(objectId);
			NextUISocket.Respond(socket, ev, DataConverter.ActionToObject(action));
		}
	}
}