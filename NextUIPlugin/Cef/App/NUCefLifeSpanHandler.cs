﻿using System;
using Dalamud.Logging;
using Xilium.CefGlue;

namespace NextUIPlugin.Cef.App {
	// ReSharper disable once InconsistentNaming
	public class NUCefLifeSpanHandler : CefLifeSpanHandler {
		public event Action<CefBrowser>? AfterBrowserLoad;
		public event Action<CefBrowser>? AfterBrowserPopupLoad;

		protected override void OnAfterCreated(CefBrowser browser) {
			if (browser.IsPopup) {
				NextUIPlugin.NULog.Information($"Browser popup created {browser.IsValid}");
				AfterBrowserPopupLoad?.Invoke(browser);
				return;
			}

			NextUIPlugin.NULog.Information($"Browser created {browser.IsValid}");
			AfterBrowserLoad?.Invoke(browser);
		}
	}
}