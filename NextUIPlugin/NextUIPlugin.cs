﻿using System.Security;
using Dalamud.Data;
using Dalamud.Game;
using Dalamud.Game.ClientState;
using Dalamud.Game.ClientState.Objects;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.ClientState.Party;
using Dalamud.Game.Command;
using Dalamud.Game.Gui;
using Dalamud.Game.Network;
using Dalamud.IoC;
using Dalamud.Logging;
using Dalamud.Plugin;
using Dalamud.Plugin.Services;
using Lumina.Excel;
using Lumina.Excel.Sheets;
using Newtonsoft.Json;
using NextUIPlugin.Configuration;
using NextUIPlugin.Data;
using NextUIPlugin.Gui;
using NextUIPlugin.Service;
using NextUIPlugin.Socket;
// using Condition = Dalamud.Game.ClientState.Conditions.Condition;

[assembly:AllowPartiallyTrustedCallers]
namespace NextUIPlugin {
	// ReSharper disable once InconsistentNaming
	// ReSharper disable once ClassNeverInstantiated.Global
	public class NextUIPlugin : IDalamudPlugin {
		public string Name => "NextUIPlugin";

		public static NextUIConfiguration configuration = null!;

		/** Dalamud injected services */
		// ReSharper disable InconsistentNaming
		// ReSharper disable ReplaceAutoPropertyWithComputedProperty
		[PluginService] public static ICommandManager commandManager { get; set; } = null!;
		[PluginService] public static IDalamudPluginInterface pluginInterface { get; set; } = null!;
		[PluginService] public static IObjectTable objectTable { get; set; } = null!;
		[PluginService] public static IFramework framework { get; set; } = null!;
		[PluginService] public static IGameNetwork gameNetwork { get; set; } = null!;
		[PluginService] public static IClientState clientState { get; set; } = null!;
		[PluginService] public static IDataManager dataManager { get; set; } = null!;
		[PluginService] public static IGameInteropProvider gameInteropProvider { get; set; } = null!;
		[PluginService] public static ITargetManager targetManager { get; set; } = null!;
		[PluginService] public static ICondition condition { get; set; } = null!;
		[PluginService] public static IPartyList partyList { get; set; } = null!;
		[PluginService] public static ISigScanner sigScanner { get; set; } = null!;
		[PluginService] public static IChatGui chatGui { get; set; } = null!;
		[PluginService] public static IGameGui gameGui { get; set; } = null!;
		[PluginService] public static IPluginLog NULog { get; set; } = null!;
		// ReSharper enable InconsistentNaming
		// ReSharper enable ReplaceAutoPropertyWithComputedProperty

		/** Internal services */
		public static MouseOverService mouseOverService = null!;
		public static GuiManager guiManager = null!;
		public static NextUISocket socketServer = null!;

		public readonly DataHandler dataHandler;
		public readonly NetworkHandler networkHandler;
		public static ExcelSheet<BNpcName>? npcNameSheet;

		public NextUIPlugin() {
			pluginInterface.UiBuilder.DisableCutsceneUiHide = true;

			npcNameSheet = dataManager.GetExcelSheet<BNpcName>();
			mouseOverService = new MouseOverService();

			configuration = pluginInterface.GetPluginConfig() as NextUIConfiguration ?? new NextUIConfiguration();
			configuration.PrepareConfiguration();
			NULog.Information(JsonConvert.SerializeObject(configuration));

			pluginInterface.UiBuilder.OpenConfigUi += OnOpenConfigUi;
			pluginInterface.UiBuilder.Draw += Render;

			socketServer = new NextUISocket(configuration.socketPort);
			socketServer.Start();

			dataHandler = new DataHandler();
			networkHandler = new NetworkHandler();

			guiManager = new GuiManager();
			guiManager.Initialize(pluginInterface);

			MicroPluginService.Initialize();
			IPCService.InitIpc();

			commandManager.AddHandler("/nu", new CommandInfo(OnCommandDebugCombo) {
				HelpMessage = "Open NextUI Plugin configuration. \n" +
				              "/nu toggle → Toggles all visible overlays.",
				ShowInHelp = true
			});
		}

		public void OnOpenConfigUi() {
			ConfigWindow.isConfigOpen = true;
		}

		public void Render() {
			guiManager?.Render();
			MicroPluginService.DrawProgress();
			MicroPluginService.DrawWarningWindow();
			if (!ConfigWindow.isConfigOpen) {
				return;
			}

			ConfigWindow.RenderConfig();
		}

		public void Dispose() {
			commandManager.RemoveHandler("/nu");
			dataHandler.Dispose();
			socketServer.Dispose();
			guiManager?.Dispose();
			MicroPluginService.Shutdown();
			IPCService.Deinit();
			mouseOverService.Dispose();
		}

		protected void OnCommandDebugCombo(string command, string arguments) {
			string[] argumentsParts = arguments.Split();

			switch (argumentsParts[0]) {
				case "toggle":
					guiManager!.ToggleOverlays();
					break;
				case "reload":
					guiManager!.ReloadOverlays();
					break;
				default:
					ConfigWindow.isConfigOpen = true;
					break;
			}

			pluginInterface.SavePluginConfig(configuration);
		}

		public static IGameObject? SearchObjectTable(uint entityId) {
			foreach (var obj in objectTable) {
				if (obj.EntityId == entityId) {
					return obj;
				}
			}

			return null;
		}
	}
}